package com.tw;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NormalParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-


    @Override
    public ParkingTicket park(Car car) {

        ParkingResult parkingResult = new ParkingResult("");
        for (ParkingLot parkingLot: getParkingLots()) {
            try {
                parkingResult = new ParkingResult(parkingLot.park(car));
                break;
            } catch (ParkingLotFullException e) {
                setLastErrorMessage("The parking lot is full.");
            }
        }
        return parkingResult.getTicket();

    }
    @Override
    public Car fetch(ParkingTicket ticket) {

        if (ticket == null) {
            setLastErrorMessage("Please provide your parking ticket.");
            return null;
        }

        for (ParkingLot parkingLot : getParkingLots()) {
            try {
                return parkingLot.fetch(ticket);
            } catch (InvalidParkingTicketException e) {
                setLastErrorMessage("Unrecognized parking ticket.");
            }
        }
        return null;
    }
    // --end->
}
