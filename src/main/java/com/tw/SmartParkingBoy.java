package com.tw;

public class SmartParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        ParkingResult parkingResult = new ParkingResult("");
        ParkingLot mostEmptyLot = new ParkingLot();

        int max = 0;
        for (ParkingLot parkingLot : getParkingLots()) {
            if (parkingLot.getAvailableParkingPosition() >= max){
                max = parkingLot.getAvailableParkingPosition();
                mostEmptyLot = parkingLot;
            }
        }
            try {
                parkingResult = new ParkingResult(mostEmptyLot.park(car));
            } catch (ParkingLotFullException e) {
                setLastErrorMessage("The parking lot is full.");
            }
        return parkingResult.getTicket();
    }

    @Override
    public Car fetch(ParkingTicket ticket) {

        if (ticket == null) {
            setLastErrorMessage("Please provide your parking ticket.");
            return null;
        }

        for (ParkingLot parkingLot : getParkingLots()) {
            try {
                return parkingLot.fetch(ticket);
            } catch (InvalidParkingTicketException e) {
                setLastErrorMessage("Unrecognized parking ticket.");
            }
        }
        return null;
    }
    // --end->
}
